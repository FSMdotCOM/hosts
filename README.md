## **What is a hosts file?**

A hosts file, named `hosts` (with no file extension), is a plain-text file used by all operating systems to map hostnames to IP addresses.

In most operating systems, the `hosts` file is preferential to DNS. Therefore if a domain name is resolved by the `hosts` file, the request never leaves your computer.

Having a smart hosts file goes a long way towards blocking malware, adware, and other irritants.

## Location of your hosts file

To modify your current `hosts` file, look for it in the following places and modify it with a text editor.

- **macOS (until 10.14.x macOS Mojave), iOS, Android, Linux**: `/etc/hosts` file.
- **macOS Catalina and above:** `/private/etc/hosts` file.
- **Windows**: `%SystemRoot%\system32\drivers\etc\hosts` file.

## **Reloading hosts file**

Your operating system will cache DNS lookups. You can either reboot or run the following commands to manually flush your DNS cache once the new hosts file is in place.

### **Windows**

Open a command prompt with administrator privileges and run this command: `ipconfig /flushdns`

### **Linux**

Open a Terminal and run with root privileges:

- **Debian/Ubuntu** `sudo service network-manager restart`
- **Linux Mint** `sudo /etc/init.d/dns-clean start`
- **Linux with systemd**: `sudo systemctl restart network.service`
- **Fedora Linux**: `sudo systemctl restart NetworkManager.service`
- **Arch Linux/Manjaro with Network Manager**: `sudo systemctl restart NetworkManager.service`
- **Arch Linux/Manjaro with Wicd**: `sudo systemctl restart wicd.service`
- **RHEL/Centos**: `sudo /etc/init.d/network restart`
- **FreeBSD**: `sudo service nscd restart`

To enable the `nscd` daemon initially, it is recommended that you run the following commands:

- sudo sysrc nscd_enable="YES"
- sudo service nscd start

Then modify the `hosts` line in your `/etc/nsswitch.conf` file to the following: `hosts: cache files dns`


**Others**: Consult [this Wikipedia article](https://en.wikipedia.org/wiki/Hosts_%28file%29#Location_in_the_file_system)


### **macOS**

Open a Terminal and run: `sudo dscacheutil -flushcache;sudo killall -HUP mDNSResponder`

**NOTE:** The Google Chrome browser may require manually cleaning up its DNS Cache on `chrome://net-internals/#dns` page to thereafter see the changes in your hosts file. See: [https://superuser.com/questions/723703](https://superuser.com/questions/723703)




**Resources**: 

 - [FileterLists](https://filterlists.com/)
 - [someonewhocares](https://someonewhocares.org/hosts/)
 - [The Big Blocklist Collection](https://firebog.net/)
 - [Steven Black](https://github.com/StevenBlack/hosts)
 
 **Tools**:
 
 - [hBlock](https://www.funkyspacemonkey.com/how-to-use-the-hosts-file-on-linux-to-block-ads-tracking-malware-domains-and-annoyances): a POSIX-compliant shell script, designed for Unix-like systems, gets a 
list of domains that serve ads, tracking scripts and malware from 
multiple sources and creates a hosts file (alternative formats are also 
supported) that prevents your system from connecting to them.
- [GasMask](https://github.com/2ndalpha/gasmask): hosts file manager for macOS. Not perfect but it works.

